#![feature(convert)]

extern crate docopt;
extern crate rustc_serialize;

use docopt::Docopt;
use rustc_serialize::{Decodable, Decoder};
use std::net::SocketAddr;
use std::str::FromStr;

static USAGE: &'static str = "
Usage:
  routing [<peer>...]
  routing --node [<peer>...]
  routing --help

If no arguments are passed, this will try to connect to an existing network
using Crust's discovery protocol.  If this is unsuccessful, you can provide
a list of known endpoints (other running instances of this example) and the node
will try to connect to one of these in order to connect to the network.

Options:
  -n, --node  Run as a RoutingNode rather than a RoutingClient.
  -h, --help  Display this help message.
";

#[derive(RustcDecodable, Debug)]
struct Args {
    arg_peer: Vec<PeerEndpoint>,
    flag_node: bool,
    flag_help: bool,
}

#[derive(Debug)]
enum PeerEndpoint {
    Tcp(SocketAddr),
}

impl Decodable for PeerEndpoint {
    fn decode<D: Decoder>(decoder: &mut D)->Result<PeerEndpoint, D::Error> {
        let str = try!(decoder.read_str());
        let address = match SocketAddr::from_str(&str) {
            Ok(addr) => addr,
            Err(_) => {
                return Err(decoder.error(format!(
                    "Could not decode {} as valid IPv4 or IPv6 address.", str).as_str()));
            },
        };
        Ok(PeerEndpoint::Tcp(address))
    }
}


fn main() {
    let args: Args = Docopt::new(USAGE)
                            .and_then(|docopt| docopt.decode())
                            .unwrap_or_else(|error| error.exit());

    println!("args: {:?}", args);
}
